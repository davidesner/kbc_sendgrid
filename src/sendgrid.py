"__author__ = 'Leo Chan'"
"__credits__ = 'Keboola 2018'"
"__project__ = 'kbc_sendgrid'"

"""
Python 3 environment 
"""

import sys
import os
import logging
import csv
import json
import requests
import datetime
import dateparser
import copy
import pandas as pd
import urllib.parse as url_parse
import logging_gelf.formatters
import logging_gelf.handlers
from keboola import docker


### Environment setup
abspath = os.path.abspath(__file__)
script_path = os.path.dirname(abspath)
os.chdir(script_path)

### Logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s',
    datefmt="%Y-%m-%d %H:%M:%S")


### Access the supplied rules
cfg = docker.Config('/data/')
params = cfg.get_parameters()
api_token = cfg.get_parameters()["#api_token"]

### QuickBooks Parameters
BASE_URL = "https://api.sendgrid.com/v3/"

### Request Parameters
requesting = requests.Session()


class sendgrid():
    """
    SendGrid Requests Handler
    """

    def __init__(self, endpoint):
        ### Parameters
        self.endpoint = endpoint
        self.run_time = dateparser.parse("now")
        self.request_timestamp_start = self.run_time - datetime.timedelta(days=1, hours=2)
        self.request_timestamp_end = self.run_time - datetime.timedelta(days=1)
        logging.info("Request Timestamp: {0} TO {1}".format(
            (self.request_timestamp_start).strftime("%Y-%m-%dT%H:00:00Z"),
            (self.request_timestamp_end).strftime("%Y-%m-%dT%H:00:00Z")
            ))
        self.header = {
            "Authorization": "Bearer {0}".format(api_token)
        }
        
        ### Runs
        ### Messages request
        messages_param = {}
        messages_param["query"] = self.url_encode("last_event_time BETWEEN TIMESTAMP \"{0}\" AND \"{1}\"".format(
            (self.request_timestamp_start).strftime("%Y-%m-%dT%H:00:00Z"),
            (self.request_timestamp_end).strftime("%Y-%m-%dT%H:00:00Z")
        ))
        print(messages_param)
        
        self.request(BASE_URL+endpoint+"?limit=99999", self.header, messages_param)


    def request(self, url, header, param):
        """
        Universal Request function
        """
        
        logging.info("Request URL: {0}".format(url))
        logging.info("Request Parameters: {0}".format(param))
        data = requesting.get(url, headers=header, data=json.dumps(param))
        print(data.text)

        return

    def url_encode(self, query):
        """
        URL encoded the query parameter
        """

        out = url_parse.quote(query)

        return out